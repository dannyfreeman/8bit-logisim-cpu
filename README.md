# Overview

This repo has code for dumping out rom contents for my 8bit logisim CPU's control unit ROM.
It also contains a primitive assembler to make it easier to write programs for the 8bit cpu.

The assembler shares the same code used to build the control unit ROM contents,
so as long as the control unit ROM is kept in sync with what is in this codebase
the assembler should work. This also prevents me from having to know anything
about how mnemonics map instructions, allow me to quickly change the machine code
and add new instructions.

Logisim doesn't have any mechanism for importing RAM contents, 
so once it is assembled the bit's have to be typed in manually.
The CPU starts looking for instructions at address 0x00
which is where the first byte of machine code should be entered into the RAM.

# The Machine Language

The machine language uses 8 bit instructions. 
Optinally, some can have 8 bit operands 
(usually memory addresses or literal numbers that can be loaded into a register).

A single instruction may use 1 or 2 bytes of memory (256 bytes of memory are available).
1 byte instructions (no operand) take 2 clock cycles to fetch.
2 byte instructions take 3 clock cycles to fetch. 2 for the instruction,
and 1 more to make the operand available in memory.
On the 4th clock cycle, the instruction will be able to put the operand on the bus 
and transfer it somewhere,
whether that be a register, memory, the program counter depends on the instruction.

Instructions currently have access to 2 flags, the carry flag and the zero flag,
which are stored in the flags register when carrying out certain instructions (add, subtract).
Using these flags, the machine code can make conditional jumps 
based on the result of the last mathematical operation.

The assembler supports labels so you don't have to calculate the address for each instruciton
in the program being written.

See comment block in `df.assembler` namespace for example program to create fibonacci numbers.

# The computer

![Screenshot of Logisim 8-bit CPU](resources/cpu-screenshot.png "Screenshot of Logisim 8-bit CPU")

The only "input" for the computer is ability to manually set bytes in the ram.
There is a single output register (register D) hooked up to a logisim display.
So if some value should be displayed to the user, it can be transfered to the D register.

The cpu.circ file can be loaded into logism.

CPU microcode needs to be created with the clojure code here and loaded into the CPU ROM from logisim. The CPU ROM can be found in the "Control Module" sub-circuit.

Run `(df.core/dump-control-unit-rom)` from a repl to create the microcode file.
