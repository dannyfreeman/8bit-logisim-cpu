(ns df.bit-utils)

(def ushort unchecked-short)

(defn | [& bytes]
  (ushort (apply bit-or bytes)))
