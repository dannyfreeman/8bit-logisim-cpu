(ns df.control-unit-assembler
  (:require
   [df.bit-utils :refer [ushort]]
   [df.constants :as C]
   [df.control-word :as cw]
   [df.instruction :as instruction]
   [df.logisim-rom-fmt :as rom-fmt]))

(defn address
  [instruction step flags]
  (short (bit-or
           (bit-shift-left (ushort instruction) 8)
           (bit-shift-left (ushort step) 5)
           (ushort flags))))

(defn rom-contents
  []
  (let [rom-bin (short-array (C/exp 2 16))]
    (doseq [i (range 0 (count instruction/op-code->control-words))]
      (let [{:keys [name steps flags]} (get instruction/op-code->control-words i)]
        (println name (format "op code: %02x" i))
        (doseq [flag-i (range 0 C/flags-counter-max)]
          (doseq [step (range 0 C/instruction-counter-max)]
            (aset-short
              rom-bin
              (address i step flag-i)
              (or ;; Get the control word for this step of the instruction
                ;; If there is a flag override, use it.
                (get-in flags [step flag-i])
                ;; If no control word exists, start the next instruction cycle.
                (get steps step cw/NEXT-CYCLE)))))))
    (rom-fmt/short-arr->rom-contents rom-bin)))

