(ns df.instruction
  "Instructions are defined below by their op-code mnemonics
   Later these instructions will be addressed by their binary op-codes.
   These values be output to the control unit."
  (:require
   [df.constants :as C]
   [df.bit-utils :refer [ushort |]]
   [df.control-word :as cw]))

(def ZERO-FLAG
  2r00001)

(def CARRY-FLAG
  2r00010)

(def ZERO-AND-CARRY
  2r00011)

(defn op-with-operand
  "Ends with operand at the current RAM address.
  Grab it with RAM->BUS"
  [instructions]
  (into [cw/FETCH cw/LOAD cw/FETCH-OP cw/PC-ENABLE] instructions))

(defn op
  [instructions]
  (into [cw/FETCH cw/LOAD] instructions))

(defn- flag-overrides
  "2d flag override vector, indexed by [step][flags-address]"
  [default-value]
  (let [flag-address->override
        (constantly (mapv (constantly default-value) (range 0 C/flags-counter-max)))]
    (mapv flag-address->override (range 0 C/instruction-counter-max))))

(def ^:private default-flags
  (flag-overrides nil))

(defn- create
  "Accepts a vector of control words to be executed at each step.
  Returns a map with those steps and a 2d array of empty flag overrides for each step/flag combination.
  nil[8][32]"
  [name operand? & steps]
  (let [i {:name name
           :steps (if operand?
                    (op-with-operand steps)
                    (op steps))
           :flags default-flags
           :operand? operand?}]
    (print name)
    (mapv #(print (format " | %04x" %)) steps)
    (println " |")
    i))

(println "Instructions + Steps")

(def NOP
  "Do nothing for an instruction cycle."
  (create :NOP false cw/NEXT-CYCLE))

(def HLT
  "Halt the computer"
  ;; Every possible input should raise the halt signal.
  ;; (instruction counter, flag)
  (apply create :HLT false (mapv (constantly cw/HALT)
                                 (range 0 C/instruction-counter-max))))

(def LDA
  "Load value from memory into register A"
  (create :LDA true
          (| cw/RAM->BUS cw/BUS->ADDR)
          (| cw/RAM->BUS cw/BUS->A)))

(def LDB
  "Load value from memory into register B"
  (create :LDB true
          (| cw/RAM->BUS cw/BUS->ADDR)
          (| cw/RAM->BUS cw/BUS->B)))

(def LIA
  "Load immediate value into register A"
  (create :LIA true (| cw/RAM->BUS cw/BUS->A)))

(def LIB
  "Load immediate value into register B"
  (create :LIB true (| cw/RAM->BUS cw/BUS->B)))

(def TAB
  "Transfer contents of register A to register B"
  (create :TAB false (| cw/A->BUS cw/BUS->B)))

(def TAD
  "Transfer contents of register A to register D / Display"
  (create :TAD false (| cw/A->BUS cw/BUS->D)))

(def TBA
  "Transfer contents of register A to register B"
  (create :TBA false (| cw/B->BUS cw/BUS->A)))

(def TBD
  "Transfer contents of register B to register D / Display"
  (create :TBD false (| cw/B->BUS cw/BUS->D)))

(def TDA
  "Transfer contents of register D to A"
  (create :TDA false (| cw/D->BUS cw/BUS->A)))

(def TDB
  "Transfer contents of register D to B"
  (create :TDB false (| cw/D->BUS cw/BUS->B)))

(def ADD
  "Add A and B, then store the result in A."
  ;; read flags first, then transfer
  (create :ADD false cw/FLAGS-IN (| cw/ALU->BUS cw/BUS->A)))

(def SUB
  "Subtract B from A, then store the result in A."
  (create :SUB false (| cw/ALU->BUS cw/BUS->A cw/FLAGS-IN cw/SUBTRACT)))

(def STA
  "Store contents of register A into RAM."
  (create :STA true
          (| cw/RAM->BUS cw/BUS->ADDR)
          (| cw/A->BUS cw/BUS->RAM)))

(def STB
  "Store contents of register B into RAM."
  (create :STB true
          (| cw/RAM->BUS cw/BUS->ADDR)
          (| cw/B->BUS cw/BUS->RAM)))

(def STD
  "Store contents of register D into RAM"
  (create :STD true
          (| cw/RAM->BUS cw/BUS->ADDR)
          (| cw/D->BUS cw/BUS->RAM)))

(def JMP
  "Jump to a new instruction."
  {:name :JMP
   :steps (op [cw/FETCH-OP (| cw/RAM->BUS cw/JMP)])
   :flags default-flags
   :operand? true})

(def DIR
  "Display RAM Contents"
  (create :DIR true (| cw/RAM->BUS cw/BUS->D)))

(def JC
  "Jump if carry flag is set"
  ;; On step 4, if CARRY flag is not set, go to the next cycle
  ;; If it is set, JMP to the operand.
  (-> (create :JC true cw/NEXT-CYCLE)
      (assoc-in [:flags 4 CARRY-FLAG]     (| cw/RAM->BUS cw/JMP))
      (assoc-in [:flags 4 ZERO-AND-CARRY] (| cw/RAM->BUS cw/JMP))))

(def JZ
  "Jump if zero flag is set"
  ;; On step 4 if ZERO flag is not set go to the next cycle
  ;; If it is set JMP to the operand.
  (-> (create :JZ true cw/NEXT-CYCLE)
      (assoc-in [:flags 4 ZERO-FLAG]      (| cw/RAM->BUS cw/JMP))
      (assoc-in [:flags 4 ZERO-AND-CARRY] (| cw/RAM->BUS cw/JMP))))

(def op-code->control-words
  "A vector of instructions indexed by their 8-bit op-code.
  Remember - Order determines the op code. Changing order
  changes the machine language."
  [NOP
   HLT
   LDA
   LDB
   LIA
   LIB
   TAB
   TAD
   TBA
   TBD
   TDA
   TDB
   ADD
   SUB
   STA
   STB
   STD
   JMP
   DIR
   JC
   JZ])

(def instruction->op-code+operand?
  "Given an instruction's mnemonic, return a map with the op-code
  and if the instruction has an operand."
  (->> op-code->control-words
       (map-indexed (fn [op-code {:keys [name operand?]}]
                      [name {:op-code op-code
                             :operand? operand?}]))
       (into {})))
