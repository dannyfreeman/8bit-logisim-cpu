(ns df.logisim-rom-fmt)

(def logisim-rom-header
  "v2.0 raw\n")

(defn short-arr->rom-contents
  [arr]
  (let [sb (StringBuilder.)]
    (.append sb logisim-rom-header)
    (doseq [word arr]
      (.append sb (format "%04x\n" word)))
    (.toString sb)))
