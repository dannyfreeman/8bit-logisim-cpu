(ns df.assembler
  (:require
   [df.instruction :refer [instruction->op-code+operand?]]))

(defn i
  [& [instruction op-or-label maybe-label]]
  (let [{:keys [operand?]} (instruction->op-code+operand? instruction)]
    (if operand?
      (cond-> [instruction op-or-label]
        (keyword? maybe-label) (with-meta {:label maybe-label}))
      (cond-> [instruction]
        (keyword? op-or-label) (with-meta {:label op-or-label})))))

(defn- instruction->machine-code
  [{:keys [address instructions]} [instruction operand :as line]]
  (let [{:keys [op-code operand?]} (instruction->op-code+operand? instruction)
        {:keys [label]} (meta line)
        next-instruction (cond-> (if operand?
                                   [op-code operand]
                                   [op-code])
                           label (with-meta {label address}))
        next-address (+ address (if operand? 2 1))]
    {:address next-address
     :instructions (conj instructions next-instruction)}))

(defn assert-unique-labels
  [labels]
  (if-let [dups (->> (frequencies labels)
                     (remove (fn [[_ ct]]
                               (= 1 ct)))
                     (map (fn [[label line]]
                            {:label label
                             :line-number line}))
                     (seq))]
    (throw (ex-info "Duplicate label detected" {:duplicates dups}))
    labels))

(defn labels->lines
  [instructions]
  (->> instructions
       (keep meta)
       (reduce merge)
       (assert-unique-labels)))
;; => #'df.assembler/labels->lines

(defn print-machine-code
  [machine-code]
  (println)
  (println "Ram contents, starting at address 0x00")
  (doseq [[op operand] machine-code]
    (print
      (if operand
        (format "%02x %02x " op operand)
        (format "%02x " op))))
  (println))

(defn assemble
  [& instructions]
  (let [instructions' (->> instructions
                           (reduce instruction->machine-code
                                   {:address 0 :instructions []})
                           :instructions)
        label->line (labels->lines instructions')
        machine-code
        (for [[op operand :as i] instructions']
          (if operand
            [op (if (keyword? operand)
                  (label->line operand)
                  operand)]
            i))]
    (print-machine-code machine-code)
    machine-code))

(comment
  ;; Sequence through fibonacci numbers
  ;; start over when we overflow
  (assemble
    (i :LIA 0 :init)
    (i :LIB 1)
    (i :TAD)
    (i :ADD :loop) ;; A = n, B = n-1, n-2 is gone
    (i :JC  :init) ;; If we overflow, start over.
    (i :TAD)       ;; No overflow, display and setup for the next calculation
    (i :TBA)       ;; A = n-2
    (i :TDB)       ;; B = n-1
    (i :JMP :loop))

  0x04 0x00 0x05 0x01 0x07 0x0c 0x13 0x00 0x07 0x08 0x0b 0x11 0x05
  ,)
