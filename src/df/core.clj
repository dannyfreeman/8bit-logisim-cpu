(ns df.core
  (:require
   [clojure.java.io :as io]
   [df.control-unit-assembler :as cu-assembler]))

(defn dump-control-unit-rom
  []
  (-> (io/file "control-unit-rom.bin")
      (spit (cu-assembler/rom-contents))))


(defn -main
  [& _args]
  (dump-control-unit-rom)
  ,)
