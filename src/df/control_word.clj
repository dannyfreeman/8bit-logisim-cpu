(ns df.control-word
  (:require
   [df.bit-utils :refer [ushort |]]))

(def A->BUS   (ushort 0x0001))
(def B->BUS   (ushort 0x0002))
(def ALU->BUS (ushort 0x0003))
(def PC->BUS  (ushort 0x0004))
(def RAM->BUS (ushort 0x0005))
(def D->BUS   (ushort 0x0006))
(def HALT     (ushort 0x00FF))

; (def unused0->bus  (ushort 0x0007))
; (def unsued1->BUS  (ushort 0x0008))
; (def unsued2->BUS  (ushort 0x0009))
; (def unsued3->BUS  (ushort 0x000A))
; (def unsued4->BUS  (ushort 0x000B))
; (def unsued5->BUS  (ushort 0x000C))
; (def unsued6->BUS  (ushort 0x000D))
; (def unsued7->BUS  (ushort 0x000E))

(def FLAGS-IN   (ushort 0x0010))
(def PC-ENABLE  (ushort 0x0020))
(def SUBTRACT   (ushort 0x0040))
(def JMP        (ushort 0x0080))
(def BUS->D     (ushort 0x0100))
(def BUS->INST  (ushort 0x0200))
(def BUS->RAM   (ushort 0x0400))
(def BUS->ADDR  (ushort 0x0800))
(def BUS->A     (ushort 0x1000))
(def BUS->B     (ushort 0x2000))
(def _UNUSED    (ushort 0x4000)) ;; short overflows and throws an error
(def NEXT-CYCLE (ushort 0x8000))

;; Common instructions used at the beginning of each instruction cycle.
;; These load/fetch instructions (and their operands) from memory.
;; New hardware could be used (dedicated memory buses, dual-channel memory)
;; to reduce the fetch/load steps to always be 2 clock cycles long.

(def FETCH
  "Load address for next instruction"
  (| PC->BUS
     BUS->ADDR))

(def LOAD
  "Load next instruction into instruction register."
  (| RAM->BUS BUS->INST PC-ENABLE)) 

(def FETCH-OP
  "Load address for instruction operand"
  (| PC->BUS BUS->ADDR))

