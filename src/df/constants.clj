(ns df.constants)

(defn exp [x n]
  (reduce * (repeat n x)))

(def instruction-bits
  (exp 2 8))

(def instruction-counter-max
  (exp 2 3))

(def flags-counter-max
  (exp 2 5))
